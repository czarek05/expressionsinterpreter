# Expressions Interpreter

Interpreter visualizes provided expression and performs a search in the data to find records, which match the expression. 

The data is a set of records. A record is an object with 8 integer attributes: a, b, c, d, e, f, g, h.

The expression may contain round brackets, logical operators (words: or, and) and comparsion operators (>, >=, <, <=, !=, =).

The project objective was not to use regular expressions. Java reflection and Reverse Polish notation (RPN) were used. RPN helped translate expression passed by user to a more friendly for computing form.

Tree printing works only in consoles, which do not add line breaks. So it is better to run the app in e.g. `IntelliJ` console rather than `cmd`.
## Usage
Build project:

`mvn clean install`

Run app:

`java -jar ./target/expressions-interpreter.jar`


Example of use:
```
Type an expression, and then press Enter (press 'q', and then press Enter to close the app): 
a > 300 or (b > 300 and c >= 300)
                                or
                                |
                ________________________________
                |                               |
                a>300                           and
                                                |
                                        ________________
                                        |               |
                                        b>300           c>=300
Would you like to select records based on the tree below? (y/n)
y
RESULTS:
Record [a=904, b=87, c=-476, d=-291, e=-202, f=-753, g=-819, h=619]
Record [a=535, b=223, c=257, d=-247, e=-970, f=615, g=151, h=-228]
Record [a=478, b=737, c=-913, d=794, e=279, f=893, g=-820, h=-236]
Record [a=320, b=907, c=699, d=131, e=899, f=941, g=-303, h=-211]
END OF RESULTS
Type an expression, and then press Enter (press 'q', and then press Enter to close the app): 
q
Process finished with exit code 0
```
