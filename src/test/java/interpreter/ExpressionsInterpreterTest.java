package interpreter;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExpressionsInterpreterTest {

    @Test
    public void testToRpnConversion() {
        String expression = "((a > 300 or b > 300)) or h = 100";
        String rpnExpression = ExpressionsInterpreter.expToRpnExp(expression);
        assertEquals(rpnExpression, "a>300b>300orh=100or");
        assertEquals(ExpressionsInterpreter.mappedRpnExp, "01or2or");

    }

    @Test
    public void testToRpnConversion2() {
        String expression = "(a>300 and (b >=    907 or b <= 87)) or (c <= 254 and (d > 400 or h	!=356)) or e > 600";
        String rpnExpression = ExpressionsInterpreter.expToRpnExp(expression);
        assertEquals(rpnExpression, "a>300b>=907b<=87orandc<=254d>400h!=356orandore>600or");
        assertEquals(ExpressionsInterpreter.mappedRpnExp, "012orand345orandor6or");

    }

    @Test
    public void testFindingTreeDepth() {
        Node root = new Node();
        Node node2 = new Node();
        Node node3 = new Node();
        Node node4 = new Node();
        Node node5 = new Node();
        Node node6 = new Node();
        Node node7 = new Node();
        Node node8 = new Node();
        Node node9 = new Node();
        Node node10 = new Node();
        Node node11 = new Node();
        Node node12 = new Node();
        Node node13 = new Node();
        Node node14 = new Node();
        Node node15 = new Node();
        root.setLeft(node2);
        root.setRight(node3);
        node2.setLeft(node4);
        node2.setRight(node5);
        node3.setLeft(node6);
        node3.setRight(node7);
        node4.setLeft(node8);
        node4.setRight(node9);
        node5.setLeft(node10);
        node5.setRight(node11);
        node6.setLeft(node12);
        node6.setRight(node13);
        node7.setLeft(node14);
        node7.setRight(node15);

        Node mockNode = new Node();
        mockNode.setDepth(0);
        root.setDepth(0);
        ExpressionsInterpreter.computeTreeDepth(root, mockNode, 0);
        int depth = mockNode.getDepth() + 1;
        assertEquals(4, depth);

    }

}
