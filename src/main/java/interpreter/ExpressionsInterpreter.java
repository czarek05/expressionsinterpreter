package interpreter;

import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Main class
 */
public class ExpressionsInterpreter {

    public static final List<Character> COLUMN_NAMES = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
    public static final List<Character> COMPARISON_OPERATORS = Arrays.asList('=', '<', '>', '!');

    /**
     * Reverse Polish notation (RPN) expression, in which predicates were converted to numbers.<br>
     * For example, an expression:<br>
     * a>300b>300c>300andor<br>
     * would be converted to:<br>
     * 012andor<br>
     * The purpose of introducing this attribute was to be able to determine, if 'a', 'd' passed by user represent
     * record's attribute or logical operator (and). Related to <b>numberToRpnMap</b>
     */
    public static String mappedRpnExp;
    /**
     * Predicates mapped to numbers. Used to determine, which predicate corresponds to which number in <b>mappedRpnExp</b>
     * For example, for expression:<br>
     * a > 300 or (b > 300 and c > 300)<br>
     * the map would be:<br>numberToRpnMap
     * 0 : a>300<br>
     * 1 : b>300<br>
     * 2 : c>300<br>
     */
    public static Map<Integer, String> numberToRpnMap;

    public static void main(String[] args) {
        List<Record> dataBase = generateDataBase(10, 1000);

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Type an expression, and then press Enter (press 'q', and then press Enter to close the app): ");
            String input = scanner.nextLine();
            if (!input.equals("q")) {
                try {
                    expToRpnExp(input);
                    Node tree = createTree();
                    // need only for storing information about tree depth
                    Node mockNode = new Node();
                    mockNode.setDepth(0);
                    tree.setDepth(0);
                    computeTreeDepth(tree, mockNode, 0);
                    Integer depth = mockNode.getDepth();
                    TreePrinter.printTree(tree, depth);
                    System.out.println(
                            "Would you like to select records based on the tree below? (y/n)");
                    input = scanner.nextLine();
                    if (input.equals("y")) {
                        Predicate<Record> predicate = computePredicate();
                        List<Record> filteredDataBase = dataBase.stream().filter(predicate)
                                .collect(Collectors.toList());
                        System.out.println("RESULTS:");
                        for (Record elem : filteredDataBase) {
                            System.out.println(elem);
                        }
                        System.out.println("END OF RESULTS");
                    } else {
                        continue;
                    }
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("\nSomething's gone wrong. Check if the typed expression is correct:\n")
                            .append(input).append("\nand try again");
                    System.out.println(sb);
                }
            } else {
                break;
            }
        }
        scanner.close();

    }

    public static List<Record> generateDataBase(Integer rowNumber, Integer range) {
        Random random = new Random();
        random.setSeed(150450l);
        List<Record> dataBase = new ArrayList<>();
        for (int i = 0; i < rowNumber; i++) {
            Integer a = random.nextInt(range) * (random.nextBoolean() ? 1 : -1);
            Integer b = random.nextInt(range) * (random.nextBoolean() ? 1 : -1);
            Integer c = random.nextInt(range) * (random.nextBoolean() ? 1 : -1);
            Integer d = random.nextInt(range) * (random.nextBoolean() ? 1 : -1);
            Integer e = random.nextInt(range) * (random.nextBoolean() ? 1 : -1);
            Integer f = random.nextInt(range) * (random.nextBoolean() ? 1 : -1);
            Integer g = random.nextInt(range) * (random.nextBoolean() ? 1 : -1);
            Integer h = random.nextInt(range) * (random.nextBoolean() ? 1 : -1);
            Record record = new Record(a, b, c, d, e, f, g, h);
            dataBase.add(record);
        }
        return dataBase;
    }

    /**
     * Converts user input to Reverse Polish notation (RPN)<br>
     * For example, an expression:<br>
     * a > 300 or (b > 300 and c > 300)<br>
     * would be converted to:<br>
     * a>300b>300c>300andor<br>
     * The purpose of the conversion is to determine the order of operations
     */
    public static String expToRpnExp(String exp) {
        exp = exp.replace(" ", "");
        exp = exp.replace("\t", "");
        String simpleExp = expToSimpleExp(exp);
        StringBuilder result = new StringBuilder();
        Deque<String> stack = new ArrayDeque<String>();
        for (int i = 0; i < simpleExp.length(); i++) {
            Character ch = simpleExp.charAt(i);
            String str = Character.toString(ch);
            if (Character.isDigit(ch)) {
                result.append(ch);
            } else if (ch == '(') {
                stack.add(str);
            } else if (ch == ')') {
                while (true) {
                    String lastElem = stack.pollLast();
                    if (lastElem.equals("(")) {
                        break;
                    }
                    result.append(lastElem);
                }
            } else {

                if (stack.contains("and") || stack.contains("or")) {
                    Iterator<String> descIterator = stack.descendingIterator();
                    while (descIterator.hasNext()) {
                        String elem = descIterator.next();
                        if (elem.equals("and")) {
                            stack.removeLastOccurrence("and");
                            result.append("and");
                            break;
                        } else if (elem.equals("or")) {
                            stack.removeLastOccurrence("or");
                            result.append("or");
                            break;
                        } else if (elem.equals("(")) {
                            break;
                        }
                    }
                }

                if (ch == 'a') {
                    stack.add("and");
                    i += 2;
                } else {
                    stack.add("or");
                    i++;
                }
            }
        }

        while (!stack.isEmpty()) {
            String str = stack.pollLast();
            result.append(str);
        }
        ExpressionsInterpreter.mappedRpnExp = result.toString();
        for (int i = numberToRpnMap.size() - 1; i >= 0; i--) {
            String strElem = Integer.toString(i);
            int index = result.indexOf(strElem);
            result.replace(index, index + strElem.length(), numberToRpnMap.get(i));
        }
        return result.toString();
    }

    /**
     * Converts predicates from user input to numbers<br>
     * For example, an expression:<br>
     * a > 300 or (b > 300 and c > 300)<br>
     * would be converted to:<br>
     * 0or(1and2)<br>
     * Moreover, this method assigns value for predicates to numbers map: <b>numberToRpnMap</b>.
     */
    private static String expToSimpleExp(String exp) {
        exp = "_" + exp + "_";
        StringBuilder sb = new StringBuilder();
        Map<Integer, String> numberToRpnMap = new HashMap<>();
        int counter = 0;
        for (int i = 1; i < exp.length() - 1; i++) {
            if (COLUMN_NAMES.contains(exp.charAt(i)) && exp.charAt(i - 1) != 'n' && exp.charAt(i + 1) != 'n') {
                sb.append(exp.charAt(i));
                i++;
                while (i < exp.length() - 1) {
                    if (COMPARISON_OPERATORS.contains(exp.charAt(i)) || Character.isDigit(exp.charAt(i))
                            || exp.charAt(i) == '-') {
                        sb.append(exp.charAt(i));
                        i++;
                    } else {
                        numberToRpnMap.put(counter, sb.toString());
                        counter += 1;
                        sb = new StringBuilder();
                        break;
                    }
                }
            }
        }
        if (sb.length() > 0) {
            numberToRpnMap.put(counter, sb.toString());
        }
        ExpressionsInterpreter.numberToRpnMap = numberToRpnMap;
        String simpleExp = exp;
        for (Integer elem : numberToRpnMap.keySet()) {
            String replacement = numberToRpnMap.get(elem);
            int index = simpleExp.indexOf(replacement);
            simpleExp = simpleExp.substring(0, index).concat(elem.toString())
                    .concat(simpleExp.substring(index + replacement.length()));
        }
        simpleExp = simpleExp.substring(1, simpleExp.length() - 1);
        return simpleExp;
    }

    /**
     * Computes predicates and joins them to obtain one, composed predicate, which represents requirements arising
     * from expression from user input. Uses attributes:<b>mappedRpnExp</b> and <b>numberToRpnMap</b>
     */
    public static Predicate<Record> computePredicate() {
        Deque<Predicate<Record>> stack = new ArrayDeque<>();
        Integer counter = 0;
        for (int i = 0; i < mappedRpnExp.length(); i++) {
            if (Character.isDigit(mappedRpnExp.charAt(i))) {
                String expPart = numberToRpnMap.get(counter);
                counter++;
                String field = "get" + Character.toUpperCase(expPart.charAt(0));
                Integer relationalOperator = null;
                if (expPart.charAt(1) == '=') {
                    relationalOperator = 0;
                } else if (expPart.charAt(1) == '!') {
                    relationalOperator = 5;
                } else if (expPart.charAt(1) == '>') {
                    if (expPart.charAt(2) == '=') {
                        relationalOperator = 3;
                    } else {
                        relationalOperator = 4;
                    }
                } else if (expPart.charAt(1) == '<') {
                    if (expPart.charAt(2) == '=') {
                        relationalOperator = 2;
                    } else {
                        relationalOperator = 1;
                    }
                }

                int rightSide;
                if (Character.isDigit(expPart.charAt(2)) || expPart.charAt(2) == '-') {
                    rightSide = Integer.parseInt(expPart.substring(2));
                } else {
                    rightSide = Integer.parseInt(expPart.substring(3));
                }
                Predicate<Record> predicate = generatePredicate(field, relationalOperator, rightSide);
                stack.add(predicate);
            } else {
                Predicate<Record> p1 = stack.pollLast();
                Predicate<Record> p2 = stack.pollLast();
                Predicate<Record> newPredicate;
                if (mappedRpnExp.charAt(i) == 'a') {
                    newPredicate = p1.and(p2);
                    i += 2;
                } else {
                    newPredicate = p1.or(p2);
                    i++;
                }
                stack.add(newPredicate);
            }
        }
        return stack.getFirst();
    }

    /**
     * Creates single predicate
     *
     * @param field              which field of record should be used to create predicate, e.g. "getD"
     * @param relationalOperator information, as a number, about which comparison operator should be used
     * @param rightSide          value of the right side of comparison
     */
    private static Predicate<Record> generatePredicate(String field, Integer relationalOperator, Integer rightSide) {
        return o -> {
            Method get;
            Integer leftSide = null;
            try {
                get = Record.class.getMethod(field);
                leftSide = (Integer) get.invoke(o);
            } catch (Exception e) {

            }
            if (relationalOperator == 0) {
                return leftSide.intValue() == rightSide.intValue();
            } else if (relationalOperator == 1) {
                return leftSide.intValue() < rightSide.intValue();
            } else if (relationalOperator == 2) {
                return leftSide.intValue() <= rightSide.intValue();
            } else if (relationalOperator == 3) {
                return leftSide.intValue() >= rightSide.intValue();
            } else if (relationalOperator == 4) {
                return leftSide.intValue() > rightSide.intValue();
            }
            return leftSide.intValue() != rightSide.intValue();

        };
    }

    /**
     * The purpose of this method is to prepare data for tree printing
     */
    public static Node createTree() {

        Deque<Node> stack = new ArrayDeque<>();
        Integer counter = 0;
        for (int i = 0; i < mappedRpnExp.length(); i++) {
            Node node = new Node();
            if (Character.isDigit(mappedRpnExp.charAt(i))) {
                node.setValue(numberToRpnMap.get(counter));
                stack.add(node);
                counter++;
            } else {
                Node rightChild = stack.pollLast();
                Node leftChild = stack.pollLast();
                node.setLeft(leftChild);
                node.setRight(rightChild);
                if (mappedRpnExp.charAt(i) == 'a') {
                    node.setValue("and");
                    i += 2;
                } else {
                    node.setValue("or");
                    i++;
                }
                stack.add(node);
            }
        }
        return stack.getFirst();
    }

    /**
     * The purpose of this method is to compute tree depth for tree printing
     *
     * @param n        investigated node
     * @param mockNode store information about tree depth
     * @param depth    investigated node depth
     */
    public static void computeTreeDepth(Node n, Node mockNode, Integer depth) {

        Integer tempDepth = depth;
        if (n.getDepth() == null) {
            n.setDepth(tempDepth);
            if (mockNode.getDepth() < tempDepth) {
                mockNode.setDepth(tempDepth);
            }
        }
        if (n.getLeft() != null) {
            computeTreeDepth(n.getLeft(), mockNode, ++tempDepth);
        }

        Integer tempDepth2 = depth;
        if (n.getDepth() == null) {
            n.setDepth(tempDepth2);
            if (mockNode.getDepth() < tempDepth2) {
                mockNode.setDepth(tempDepth2);
            }
        }
        if (n.getRight() != null) {
            computeTreeDepth(n.getRight(), mockNode, ++tempDepth2);
        }
    }

}
