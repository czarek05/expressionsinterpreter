package interpreter;

import java.util.ArrayList;
import java.util.List;

public class TreePrinter {


    public static void printTree(Node root, Integer depth) {

        List<Node> firstNode = new ArrayList<>();
        Integer width = 4;
        for (int i = 0; i < depth; i++) {
            width *= 4;
        }

        firstNode.add(root);
        printNode(width, firstNode);
        Integer elementsToPrint = 1;
        List<Node> recentlyPrintedNodes = firstNode;
        for (int i = 0; i < depth; i++) {

            List<Node> nodesToPrint = new ArrayList<>();
            for (Node node : recentlyPrintedNodes) {
                if (node != null) {
                    nodesToPrint.add(node.getLeft());
                    nodesToPrint.add(node.getRight());
                } else {
                    nodesToPrint.add(null);
                    nodesToPrint.add(null);
                }
            }

            printPillar(width, elementsToPrint, nodesToPrint);

            printBar(width, elementsToPrint, nodesToPrint);

            printPillar(width, 2 * elementsToPrint, nodesToPrint);

            printNode(width, nodesToPrint);
            recentlyPrintedNodes = nodesToPrint;
            elementsToPrint *= 2;
        }

    }


    private static void printBar(Integer treeWidth, Integer barsNumber, List<Node> nodes) {
        for (int i = 0; i < treeWidth / (barsNumber * 4); i++) {
            System.out.print(" ");
        }
        Integer counter = 0;
        for (int i = 1; i < barsNumber; i++) {
            if (nodes.get(counter) != null) {
                for (int j = 0; j < treeWidth / (barsNumber * 2); j++) {
                    System.out.print("_");
                }
            } else {
                for (int j = 0; j < treeWidth / (barsNumber * 2); j++) {
                    System.out.print(" ");
                }
            }
            counter += 2;
            for (int j = 0; j < treeWidth / (barsNumber * 2); j++) {
                System.out.print(" ");
            }
        }
        if (nodes.get(nodes.size() - 1) != null) {
            for (int j = 0; j < treeWidth / (barsNumber * 2); j++) {
                System.out.print("_");
            }
        }
        System.out.println();
    }


    private static void printPillar(Integer treeWidth, Integer pillarsNumber, List<Node> nodes) {

        Integer counter = 0;
        for (int i = 0; i < treeWidth / (pillarsNumber * 2); i++) {
            System.out.print(" ");
        }
        for (int i = 1; i < pillarsNumber; i++) {
            if (nodes.get(counter) != null) {
                System.out.print("|");
            } else {
                System.out.print(" ");
            }
            if (pillarsNumber == nodes.size()) {
                counter++;
            } else {
                counter += 2;
            }
            for (int j = 0; j < treeWidth / pillarsNumber - 1; j++) {
                System.out.print(" ");
            }
        }
        if (nodes.get(nodes.size() - 1) != null) {
            System.out.print("|");
        }
        System.out.println();

    }

    private static void printNode(Integer treeWidth, List<Node> nodes) {
        Integer nodesNumber = nodes.size();
        for (int i = 0; i < treeWidth / (nodesNumber * 2); i++) {
            System.out.print(" ");
        }
        Integer shift = 0;
        for (int i = 0; i < nodesNumber - 1; i++) {
            Node node = nodes.get(i);
            if (node != null) {
                System.out.print(node.getValue());
                shift = node.getValue().length();
            } else {
                System.out.print("");
                shift = "".length();
            }

            for (int j = 0; j < treeWidth / nodesNumber - shift; j++) {
                System.out.print(" ");
            }
        }
        if (nodes.get(nodesNumber - 1) != null) {
            System.out.print(nodes.get(nodesNumber - 1).getValue());
        }
        System.out.println();

    }

}
