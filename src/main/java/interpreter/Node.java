package interpreter;

public class Node {

    private Node left;
    private Node right;
    /**
     * Stores logical operator ("or", "and") or string predicate (e.g. "a>300")
     */
    private String value;
    private Integer depth;

    public Node() {

    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

}
